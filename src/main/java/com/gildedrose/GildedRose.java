package com.gildedrose;

class GildedRose {
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String AGED_BRIE = "Aged Brie";
    public static final int MAX_QUALITY = 50;
    public static final int STAYING_QUALITY_DAYS_FACTOR_2 = 10;
    public static final int STAYING_QUALITY_DAYS_FACTOR_3 = 5;
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            if (item.name.equals(AGED_BRIE)) {
                if (item.quality < MAX_QUALITY) {
                    increaseQuality(item);
                }
            } else if (item.name.equals(BACKSTAGE_PASSES)) {
                if (item.quality < MAX_QUALITY) {
                    increaseQuality(item);
                    if (item.sellIn <= 10) {
                        if (item.quality < MAX_QUALITY) {
                            increaseQuality(item);
                        }
                    }

                    if (item.sellIn <= 5) {
                        if (item.quality < MAX_QUALITY) {
                            increaseQuality(item);
                        }
                    }
                }
            } else {
                if (item.quality > 0) {
                    if (!item.name.equals(SULFURAS)) {
                        decreaseQuality(item);
                    }
                }
            }

            if (!item.name.equals(SULFURAS)) {
                item.sellIn = item.sellIn - 1;
            }

            if (item.sellIn < 0) {
                if (!item.name.equals(AGED_BRIE)) {
                    if (!item.name.equals(BACKSTAGE_PASSES)) {
                        if (item.quality > 0) {
                            if (!item.name.equals(SULFURAS)) {
                                decreaseQuality(item);
                            }
                        }
                    } else {
                        item.quality = 0;
                    }
                } else {
                    if (item.quality < MAX_QUALITY) {
                        increaseQuality(item);
                    }
                }
            }
        }
    }

    private void decreaseQuality(Item item) {
        item.quality = item.quality - 1;
    }

    private void increaseQuality(Item item) {
        item.quality = item.quality + 1;
    }
}
